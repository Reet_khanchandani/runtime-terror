package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.StockWithTimestamp;
import com.example.demo.entities.Stocks;

@Component
public interface StocksRepository {

	public List<StockWithTimestamp> getAllStocks();

	public StockWithTimestamp getStocksById(int id);

	public Stocks editStocks(Stocks Stocks);

	public int deleteStocks(int id);

	public Stocks addStocks(Stocks Stocks);

}
