package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.StockWithTimestamp;
import com.example.demo.entities.Stocks;

@Repository
public class MyStocksRepository implements StocksRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<StockWithTimestamp> getAllStocks() {
		// TODO Auto-generated method stub
		String sql = "SELECT id, updatedTime, stockTicker, price, volume, buyOrSell, statusCode FROM stockdata";
		return (List<StockWithTimestamp>) template.query(sql, new StocksTsRowMapper());
	}

	@Override
	public StockWithTimestamp getStocksById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, updatedTime, stockTicker, price, volume, buyOrSell, statusCode FROM stockdata WHERE id=?";
		return template.queryForObject(sql, new StocksTsRowMapper(), id);
	}

	@Override
	public Stocks editStocks(Stocks Stocks) {
		// TODO Auto-generated method stub
		String sql = "UPDATE stockdata SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ? WHERE id = ?";
		template.update(sql, Stocks.getStockTicker(), Stocks.getPrice(), Stocks.getVolume(), Stocks.getBuyOrSell(),
				Stocks.getStatusCode(), Stocks.getId());
		return Stocks;
	}

	@Override
	public int deleteStocks(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM stockdata WHERE id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Stocks addStocks(Stocks Stocks) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stockdata(id, stockTicker, price, volume, buyOrSell, statusCode) "
				+ "VALUES(?,?,?,?,?,?)";
		template.update(sql, Stocks.getId(), Stocks.getStockTicker(), Stocks.getPrice(), Stocks.getVolume(),
				Stocks.getBuyOrSell(), Stocks.getStatusCode());
		return Stocks;
	}

	class StocksRowMapper implements RowMapper<Stocks> {

		@Override
		public Stocks mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Stocks(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
					rs.getString("buyOrSell"), rs.getInt("statusCode"));

		}

	}
	
	class StocksTsRowMapper implements RowMapper<StockWithTimestamp> {

		@Override
		public StockWithTimestamp mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new StockWithTimestamp(rs.getInt("id"), rs.getString("updatedtime"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
					rs.getString("buyOrSell"), rs.getInt("statusCode"));

		}

	}

}