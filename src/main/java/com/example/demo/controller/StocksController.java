package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.StockWithTimestamp;
import com.example.demo.entities.Stocks;
import com.example.demo.service.StocksService;

@RestController
@RequestMapping("api/Stocks")
public class StocksController {

	@Autowired
	StocksService service;

	@GetMapping(value = "/")
	public List<StockWithTimestamp> getAllStocks() {
		return service.getAllStocks();
	}

	@GetMapping(value = "/{id}")
	public StockWithTimestamp getStocksById(@PathVariable("id") int id) {
		return service.getStocks(id);
	}

	@PostMapping(value = "/")
	public Stocks addStocks(@RequestBody Stocks Stocks) {
		return service.newStocks(Stocks);
	}

	@PutMapping(value = "/")
	public Stocks editStocks(@RequestBody Stocks Stocks) {
		return service.saveStocks(Stocks);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStocks(@PathVariable int id) {
		return service.deleteStocks(id);
	}

}